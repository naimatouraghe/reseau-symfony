<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Message;
class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(ManagerRegistry $doctrine, Request $request, EntityManagerInterface $entityManager): Response
    { 
        $results = $doctrine->getRepository(Message::class)->findAll([], ["created_at" => "DESC","active" => true]);
        
        $messages = new Messages();
        $form = $this->createForm(QuestionFormType::class, $messages);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $messages -> setQuestion(
                $form->get('question')->getData()
            );
            $messages -> setContent(
                $form->get('content')->getData()
            );
            $entityManager->persist($messages);
            $entityManager->flush();
            return $this->redirectToRoute('app_home');
        }

        return $this->render('home/index.html.twig', [
            'result' => $results,
            'questionForm' => $form->createView(),
        ]);
    }
}
